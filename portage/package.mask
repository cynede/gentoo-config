<dev-haskell/binary-0.7.5
<dev-haskell/ghc-api-7.10.2
<dev-lang/ghc-7.10.2

#Due my lazyness
>sys-kernel/gentoo-sources-4.2.4

#systemd
sys-fs/udev
sys-fs/eudev

>=virtual/jdk-1.8.0
>=virtual/jre-1.8.0
dev-java/oracle-jdk-bin
dev-java/oracle-jre-bin

# KDE 5
kde-base/kdelibs

#=www-plugins/chrome-binary-plugins-48.0.2541.0_alpha1
